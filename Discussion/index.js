// console.log("Hello");

// ES6 Updates

// Exponent Operator

// old
console.log("Exponent Operator")
console.log("Result of old:");
const oldNum = Math.pow(8, 2);
console.log(oldNum);

// new
console.log("Result of ES6:");
const newNum = 8 ** 2;
console.log(newNum);
console.log("");

// Template Literals
/*
	-Allows us to write strings without using the concantenate operator (+)
*/
let studentName = 'Roland'


// Pre-template Literal String
console.log('Pre-tenmplate Literal');
console.log('Hello '+ studentName +'! Welcome to programming');

// Template Literal String
console.log('Template Literal');
console.log(`Hello ${studentName}! Welcome to programming!`);

// Multi-line template literal
const message = `
${studentName} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of 
${newNum}
`
console.log(message);
console.log("");

/*
	- Template literals allow us to write strings with embedded Javascript expression
	'`${}`' are used to include Javascript expression in strings using template literals
*/

const interestRate = .1;
const principal = 1000;
console.log(`The interest rate on your savings account is ${principal * interestRate}`);
console.log("");

// Array Destructuring
/*
	- Allows us to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

console.log("Array Destructuring:");

const fullName = ['Jeru', 'Nebur', 'Palma'];

// Pre-Array Destructuring
console.log("Pre-array Destructuring");
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);
console.log("");

// Array Destructuring
console.log("Array Destructuring");
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);
console.log("");

// Object Destructuring
/*
	- Allows us to unpack properties on objects into distinct values
	- Shortens the syntax for accessing properties from objects
	Syntax:
		let/const {propertyName, propertyName, propertName} = object;
*/

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// Pre Object Destructuring
console.log("Pre-Object Destructuring");
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`)

// Object Destructuring
console.log("Object Destructuring");
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}`)


// ARROW FUNCTIONS
/*
	- Compact alternative syntax to traditional functions
	- Useful for creating code snippets where creating functions will not be reused in any other portions of the code
	- Adheres to the DRY principle (Don't Repeat Yourself) principle where there is no longer a need to create a new function and think of a name for functions that will only be used in certain code snippets
*/

const hello = () => {
	// return "Good morning Batch 241"
	console.log('Hello World!')
};

/*
	let / const variableName = (parameterA, parameterB, parameterC) => {
		console.log();
	}
*/;

const printFullName = (firstN, middleN, lastN) => {
	console.log(`${firstN} ${middleN}. ${lastN}`)
}

printFullName('John', 'D', 'Smith');
printFullName('Eric', 'A', 'Andales');

const students = ["John","Jane", "Smith"]

// Arrow function with loops 
// 1 beses lang ginagamit ang function

// Old
students.forEach(function(student){
	console.log(`${student} is a student`)
})

console.log("")
// New
students.forEach((student) => {
	console.log(`${student} is a function.`)
})

// IMPLICIT RETURN STATEMENT
/*
	- There are instances when you can omit the "return" statement
	- This works because even without the return statement, Javascript adds for it for the result of the function
*/

const add = (x, y) => {
	return x + y
}

// Old / Pre - Arrow Fdunction
let total = add(1, 2)
console.log(total);


// Arrow Function
const subtract = (x, y) => x - y
let difference = subtract (3, 1)
console.log(difference)


// Instantiate a new object
// const myNewCar = new("Toyota", "Vios", 2021);
// console.log(myNewCar);


// MINI-ACTIVITY
/*
	1. Create a "dog" class
	2. Inside of the class "dog", have a name, age, and breed
	3. Instantiate a new dog class and print in the console
	4. Send the screenshot of the output on hangouts

*/

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let myDog = new Dog("Bernard", 3, "Poodle");
console.log(myDog);


























