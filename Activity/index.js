// console.log("Hello!");

// Mini-Activity
/*
	1. Using the ES6 update, get the cube of 8.
	2. Print the result on the console with the message: "The cube of 8 is" + result
	3. Use the Template Literal in printing out the message
*/

const cubeOf8 = 8 ** 3;
console.log(`The cube of 8 is ${cubeOf8} `);


// Mini Activity
/*
	1. Destructuring the address array
	2. Print the values in the console: I live at 258 Washington Ave NW, California, 90011
	3. Use template literals
	4. Send output on hangouts
*/
const address = ['258', 'Washington Ave NW', 'California', '99011']

const [houseNumber, streetName, city, zipCode] = address;

console.log(`I live at ${houseNumber}, ${streetName}, ${city}, ${zipCode}`);


// Mini-activity
/*
	1. Destructuring the animal object
	2. Print the values in the console 'Lolong was a saltwater crocodile. He weighted at 1075 kgs with a measurement of 20 ft 3 in'
	3. Use template literals
	4. Send the output on hangouts
*/

const animal = {
	name: 'Lolong',
	species: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20ft 3 in'
}


const {name, species, weight, measurement} = animal
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement} `);


// MINI ACTIVITY
/*
	1. Loop through the numbers using forEach using arrow function
	2. Print the numbers in the console
	3. Use the .reduce operator on the numbers array
	4. Assign the result on a variable
	5. Print the variable on the console
*/


let numbers = [1, 2, 3, 4, 5];

// numbers.forEach((num) => {
// 	console.log(numbers)
// });

numbers.forEach((num) => console.log(number))
let reduceNumber = numbers.reduce((x ,y) => x + y);
console.log(reduceNumber);























